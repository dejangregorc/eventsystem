package si.feri.ost.ost.demo.Models;

public class Event
{


    private int id_event;
    private String title;
    private String description;
    private int countryID;
    private int userID;
    private int typeID;
    private String location;
    private String date;
    private String time;
    private String price;

    public Event() {}

    public Event(String title)
    {

        this.title=title;
    }

    public Event(int id, String title, String description, int countryID, int userID, int typeID, String location, String date, String time, String price) {
        this.id_event = id;
        this.title = title;
        this.description = description;
        this.countryID = countryID;
        this.userID = userID;
        this.typeID = typeID;
        this.location = location;
        this.date = date;
        this.time = time;
        this.price = price;
    }

    public Event(int id, String title, String description, int countryID, int userID, int typeID)
    {
        this.id_event =id;
        this.title=title;
        this.description=description;
        this.countryID=countryID;
        this.userID=userID;
        this.typeID=typeID;

    }
    public Event(String title, String description, int countryID, int userID, int typeID)
    {

        this.title=title;
        this.description=description;
        this.countryID=countryID;
        this.userID=userID;
        this.typeID=typeID;

    }








    @Override
    public String toString() {

        return "Naziv: "+title;
    }

    public int getId() {
        return id_event;
    }

    public void setId(int id) {
        this.id_event = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCountryID() {
        return countryID;
    }

    public void setCountryID(int countryID) {
        this.countryID = countryID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
