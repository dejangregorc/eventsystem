package si.feri.ost.ost.demo.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import si.feri.ost.ost.demo.Models.Country;
import si.feri.ost.ost.demo.Models.Event;
import si.feri.ost.ost.demo.Models.SavedEvent;


@Component
public class SavedEventDAO {
    JdbcTemplate jdbcTemplate;

    public SavedEvent getById(int id)
    {
        String sql = "SELECT * FROM SAVEDEVENT WHERE id_saved_event=? ";

        SavedEvent d= (SavedEvent)jdbcTemplate.queryForObject(sql,
                new Object[] {id},
                new BeanPropertyRowMapper(SavedEvent.class));
        return d;
    }
}
