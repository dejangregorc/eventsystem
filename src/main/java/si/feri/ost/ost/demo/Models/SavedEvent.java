package si.feri.ost.ost.demo.Models;

public class SavedEvent
{
    private int id;
    private int userId;
    private int eventId;

    public SavedEvent(){}

    public SavedEvent(int userId, int eventId)
    {
        this.userId=userId;
        this.eventId=eventId;
    }

    public SavedEvent(int id, int userId, int eventId)
    {
        this.id=id;
        this.userId=userId;
        this.eventId=eventId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
}
