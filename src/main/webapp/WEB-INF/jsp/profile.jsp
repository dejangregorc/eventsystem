<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kulturnik</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/lib/css/materialize.min.css" media="screen,projection"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/lib/css/style.css">
</head>

<body class="teal lighten-5">
<main class="teal lighten-5">
    <div class="navbar-fixed">
        <nav class=" teal darken-2 z-depth-3">
            <div class="nav-wrapper">
                <a href="index" class="brand-logo">EV3NT5</a>
                <a href="#" class="button-collapse" data-activates="mobile-sidenav">
                    <i class="material-icons">menu</i>
                </a>
                <ul class="right show-on-med-and-down">
                    <li>
                        <a href="mapEvents">
                            <i class="material-icons">place</i>
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-trigger" data-activates="dropdownPerson" href="#">
                            <i class="material-icons left">person</i>
                        </a>
                        <ul id='dropdownPerson' class='dropdown-content'>
                            <%if(session.getAttribute("username")!=null)
                            {%>
                            <li>
                                <a href="profile">
                                    <button class="btn-flat teal-text">My events</button>
                                </a>

                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="addEvent">
                                    <button class="btn-flat teal-text">Add event</button>
                                </a>
                            </li>
                            <li>
                                <a href="/logout">
                                    <button class="btn-flat teal-text">Log out</button>
                                </a>
                            </li>
                            <% }else{%>
                            <li class="divider"></li>
                            <li>
                                <a href="loginPage">
                                    <button class="btn-flat teal-text">Sign in</button>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="signup">
                                    <button class="btn-flat teal-text">Sign up</button>
                                </a>
                            </li>
                            <% } %>
                        </ul>
                    </li>
                </ul>
                <ul id="nav-mobile" class="right hide-on-med-and-down">

                    <li class="active">
                        <a href="index">Home</a>
                    </li>
                    <li>
                    <li>
                        <a href="events">Events</a>
                    </li>
                </ul>
                <ul class="side-nav" id="mobile-sidenav">
                    <li>
                        <a href="events">Events</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <div class="row">
        <div class="col offset-m2 m8">
            <c:forEach items="${user}" var="user">
            <h1 class="center-align">
                <%=String.valueOf(session.getAttribute("name"))%> <%=String.valueOf(session.getAttribute("surname"))%>
            </h1>
            </c:forEach>
            <hr>
        </div>
        <div class="row">
            <div class="col offset-s1 s10">
                <div class="row">
                    <div class="col m3">
                        <div class="card-panel teal lighten-4 z-depth-1 medium center-align">
                            <div class="row">
                                <c:forEach items="${user}" var="u">
                                <img class="responsive-img z-depth-0 circle col offset-s1 s10 offset-l3 l6"
                                     src="<%=String.valueOf(session.getAttribute("avatarski"))%>" >

                                <form action="/editUser">
                                    <button class="btn-floating btn btn-flat black-text" name="eUser"
                                            value="<%=String.valueOf(session.getAttribute("idUser"))%>"><i
                                            class="material-icons right small black-text">settings</i></button>
                                </form>
                                <hr class="col offset-s1 s10">
                            </div>
                            </c:forEach>
                            <c:forEach items="${user}" var="user">
                            <span class="flow-text">
                              ${user.getName()}   ${user.getSurname()}
                                <br>username: ${user.getUsername()}
                                  <br>phone: ${user.getPhone()}
                                  <br>birth date: ${user.getBirthDate()}
                           </span>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col m9">
                        <div class="row">
                            <h2 class="center-align">
                                Added events:
                            </h2>
                            <!-- Začetek Vrstice-->
                            <!-- Začetek ENE Karte-->
                                <c:forEach items="${events}" var="event">
                                    <div class="col s12 m6 l4">
                                        <div class="card hoverable">
                                            <div class="card-image waves-effect waves-block">
                                                <img class="activator responsive-image" src="/lib/slike/${event.getId()}.jpg">
                                            </div>
                                            <div class="card-content">

                <span class="card-title activator grey-text text-darken-4">${event.getTitle()}
                <i class="material-icons right">expand_less</i>
              </span>
                    <p>
                <span class="">Location: ${event.getLocation()}
                  <br/>
                </span>
                        <a href="">more...</a>
                    </p>
                                            </div>
                                            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">
                <i class="material-icons right">expand_more</i>${event.getTitle()}</span>
                                                <p>Location: ${event.getLocation()}
                                                    <br/> Price: ${event.getPrice()}


                                                    <br/>
                                                <p class="">${event.getDescription()}
                                                    <a href=""></a>
                                                </p>
                                                <form action="/mapEvents" method="get">
                                                    <button type="submit" class="btn valign-wrapper right blue lighten-1 waves-effect" name="id" value="${event.getId()}">
                                                        <div class="valign-wrapper"> &nbsp
                                                            <i class="material-icons">location_on</i>
                                                        </div>
                                                    </button>
                                                </form>
                                                <form action="/editEvent" method="post">
                                                    <button type="submit" class="btn valign-wrapper right blue lighten-1 waves-effect" name="id" value="${event.getId()}">
                                                        <div class="valign-wrapper"> &nbsp
                                                            <i class="material-icons">edit</i>
                                                        </div>
                                                    </button>
                                                </form>
                                                    <form action ="/removeEvent" method="post">
                                                         <input type="hidden" name="idEvent" value="${event.getId()}">
                                                         <button class="btn valign-wrapper right blue lighten-1 waves-effect" value="Remove">
                                                                 <div class="valign-wrapper"> &nbsp
                                                                     <i class="material-icons">delete</i>
                                                                 </div>
                                                         </button>
                                                    </form>

                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>

                                <!-- Konec ENE Karte-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<footer class="page-footer teal lighten-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">EV3NT5</h5>
                <p class="grey-text text-lighten-4">Brought to you by a team of enthusiastic students</p>
                <p class="grey-text text-lighten-4"><a class="grey-text text-lighten-4" href="https://gitlab.com/dejangregorc/eventsystem">Check our progress on Git</a></p>
            </div>
            <div class="col l6 s12">
                <ul>
                    <li class="grey-text text-lighten-3"><h5>Brought to you by</h5></li>
                    <li class="grey-text text-lighten-3">Faculty of Informatics</li>
                    <li class="grey-text text-lighten-3">Kaunas University of Technology</li>
                    <li class="grey-text text-lighten-3">Software Engineering</li>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright Eventisi
            <a class="grey-text text-lighten-4 right" href="index">EV3NT5</a>
        </div>
    </div>
</footer>
</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="/lib/javascript/materialize.min.js"></script>


<script>
    // Every page needs this dingy //
    $(document).ready(function () {
        $('.dropdown-button').dropdown({
            constrainWidth: false,
            hover: true,
            belowOrigin: true,
            alignment: 'left'
        });
        // Navbar //
        $('.button-collapse').sideNav();
        $('.dropdown-trigger').dropdown({
            constrainWidth: false,
            hover: true
        });

        // Page Specific //

        Materialize.updateTextFields();
    });
</script>

</html>