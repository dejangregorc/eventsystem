package si.feri.ost.ost.demo.DAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import si.feri.ost.ost.demo.Models.Term;

@Component
public class TermDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;


    public Term getByEvent(int id)
    {
        String sql = "SELECT * FROM TERM WHERE event_id_event=? ";

        Term d= (Term)jdbcTemplate.queryForObject(sql,
                new Object[] {id},
                new BeanPropertyRowMapper(Term.class));
        return d;
    }
}
