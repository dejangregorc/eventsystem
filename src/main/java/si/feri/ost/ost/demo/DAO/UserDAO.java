package si.feri.ost.ost.demo.DAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import si.feri.ost.ost.demo.Models.Event;
import si.feri.ost.ost.demo.Models.User;
import si.feri.ost.ost.demo.UsefulClasses.DateTimeMethods;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class UserDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;


    public int addUser(String name, String surname, String phone, String birthDate, String username, String password, int country)
    {
        String avatar="https://api.adorable.io/avatars/200/abott@adorable.png";
        String sql = "INSERT INTO USER(name,surname,phone,username,password,country_id_country) VALUES(?,?,?,?,?,?)";

        return jdbcTemplate.update(sql,new Object[]{name,surname,phone,username,password,country});


    }

    public Object deleteUser(int id)
    {
        String sql = "DELETE FROM USER WHERE id=?";

        return jdbcTemplate.update(sql,new Object[]{id});
    }

    public List<User> getAllUsers()
    {
        String sql = "SELECT * FROM USER";
        List<User> list = new ArrayList<>();

        List<Map<String,Object>> rows  = jdbcTemplate.queryForList(sql);

        for(Map row: rows){

            int id_user = (Integer)row.get("id_user");
            String name = (String)row.get("name");
            String surname = (String)row.get("surname");
            String username = (String)row.get("username");
            String password = (String)row.get("password");
            list.add(new User(id_user,name,surname,username,password));
        }
        return list;
    }

    public List<User> getByEmail(String email)
    {
        String sql = "SELECT * FROM USER WHERE username=? ";
        List<User> list = new ArrayList<>();

        List<Map<String,Object>> rows  = jdbcTemplate.queryForList(sql,new Object[]{email});

        for(Map row: rows){

           int id_user = (Integer)row.get("id_user");
           String name = (String)row.get("name");
           String surname = (String)row.get("surname");
           String username = (String)row.get("username");
           String phone = (String)row.get("phone");
           String birth = (String)row.get("birth");
            list.add(new User(id_user,name,surname,phone,birth,username));
        }
        return list;

    }

    public User getByPassword(String password)
    {
        String sql = "SELECT * FROM USER WHERE password=? ";

        User d= (User)jdbcTemplate.queryForObject(sql,
                new Object[] {password},
                new BeanPropertyRowMapper(User.class));


        return d;

    }

    public List<User> getById(int id)
    {
        String sql = "SELECT * FROM USER WHERE id_user=? ";
        List<User> list = new ArrayList<>();
        List<Map<String,Object>> rows  = jdbcTemplate.queryForList(sql,new Object[]{id});
        for(Map row: rows){

            int id_user = (Integer)row.get("id_user");
            String name = (String)row.get("name");
            String surname = (String)row.get("surname");
            String username = (String)row.get("username");
            String phone = (String)row.get("phone");
            String birth = (String)row.get("birth");
            String pass = (String)row.get("password");
            list.add(new User(id_user,name,surname,phone,birth,username,pass));
        }
        return list;

    }

    public int updateUser(int id_user, String name, String surname, String contact, String password,  int country)
    {

        String sql = "UPDATE USER SET name=?, surname=?, phone=?, password=?, country_id_country=? WHERE id_user=?";
        return jdbcTemplate.update(sql,new Object[]{name,surname,contact,password,country,id_user});

    }
}
