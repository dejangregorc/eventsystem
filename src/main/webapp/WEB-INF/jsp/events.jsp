<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="sl">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EV3NT5</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/lib/css/materialize.min.css" media="screen,projection" />
    <link rel="stylesheet" type="text/css" media="screen" href="/lib/css/style.css" />

</head>

<body>
<main class="teal lighten-5">
    <div class="navbar-fixed">
        <nav class=" teal darken-2 z-depth-3">
            <div class="nav-wrapper">
                <a href="index" class="brand-logo">EV3NT5</a>
                <a href="#" class="button-collapse" data-activates="mobile-sidenav">
                    <i class="material-icons">menu</i>
                </a>
                <ul class="right show-on-med-and-down">
                    <li>
                        <a href="mapEvents">
                            <i class="material-icons">place</i>
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-trigger" data-activates="dropdownPerson" href="#">
                            <i class="material-icons left">person</i>
                        </a>
                        <ul id='dropdownPerson' class='dropdown-content'>
                            <%if(session.getAttribute("username")!=null)
                            {%>
                            <li>
                                <a href="profile">
                                    <button class="btn-flat teal-text">My events</button>
                                </a>

                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="addEvent">
                                    <button class="btn-flat teal-text">Add event</button>
                                </a>
                            </li>
                            <li>
                                <a href="/logout">
                                    <button class="btn-flat teal-text">Log out</button>
                                </a>
                            </li>
                            <% }else{%>
                            <li class="divider"></li>
                            <li>
                                <a href="loginPage">
                                    <button class="btn-flat teal-text">Sign in</button>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="signup">
                                    <button class="btn-flat teal-text">Sign up</button>
                                </a>
                            </li>
                            <% } %>
                        </ul>
                    </li>
                </ul>
                <ul id="nav-mobile" class="right hide-on-med-and-down">

                    <li class="active">
                        <a href="index">Home</a>
                    </li>
                    <li>
                    <li>
                        <a href="events">Events</a>
                    </li>
                </ul>
                <ul class="side-nav" id="mobile-sidenav">
                    <li>
                        <a href="events">Events</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</main>

    <div class="container">
        <h2 class="center-align">
            All events
        </h2>
        <hr/>
        <div class="row">
            <!-------------------------------------------------------------------- filters ------------------------------------------------------------->
            <form action="/filter" method="get">
                    <div class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="titleOfEvent" name="title" type="text" class="validate">
                                <label for="titleOfEvent">Title of event</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="locatOfEvent" name="location" type="text" class="validate">
                                <label for="locatOfEvent">Location of event</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input  id="dateOfEvent" name="date" type="text" class="datepicker">
                                <label for="dateOfEvent">Date:</label>
                            </div>
                            <div class="input-field col s6">
                                <label for="maxPrice">Max price</label>
                                <p class="range-field">
                                    <input type="range" id="maxPrice" name="maxPrice" min="0" max="100" />
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field center-align">
                                <button class="btn btn-submit large">
                                    <div class="valign-wrapper">
                                        Search events &nbsp <i class="material-icons suffix" type="submit">send</i>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
            </form>
            <!--------------------------------------------------------------------end filters------------------------------------------------------------->
        </div>
    </div>





    <!-- Začetek Vrstice----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <div class="row">
        <!-- Začetek ENE Karte-->
        <c:forEach items="${events}" var="event">
        <div class="col s12 m6 l4">
            <div class="card hoverable">
                <div class="card-image waves-effect waves-block">
                    <img width="200" height="200" class="activator responsive-image" src="/lib/slike/${event.getId()}.jpg">
                </div>
                <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">${event.getTitle()}
                <i class="material-icons right">expand_less</i>
              </span>
                    <p>
                <span class="">Location: ${event.getLocation()}
                  <br/>
                </span>
                        <span class="">Date: ${event.getDate()}
                  <br/>
                </span>
                        <a href="">more...</a>
                    </p>
                </div>
                <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">
                <i class="material-icons right">expand_more</i>${event.getTitle()}</span>
                    <p>Location: ${event.getLocation()}
                        <br /><span class="">Date: ${event.getDate()}
                  <br/>
                </span>
                        <span class="">Time: ${event.getTime()}
                  <br/>
                </span>
                        <br/> Price: ${event.getPrice()}


                        <br/>
                    <p class="">${event.getDescription()}
                        <a href=""></a>
                    </p>
                    <form action="/mapEvents" method="get">
                    <button type="submit" class="btn valign-wrapper right blue lighten-1 waves-effect" name="id" value="${event.getId()}">
                        <div class="valign-wrapper"> map &nbsp
                            <i class="material-icons">location_on</i>
                        </div>
                    </button>
                    </form>
                    </p>
                </div>
            </div>
        </div>
        </c:forEach>
        <!-- Konec ENE Karte-->
        <!-- Začetek ENE Karte-->
       <!-- <div class="col s12 m6 l4">
            <div class="card hoverable">
                <div class="card-image waves-effect waves-block">
                    <img class="activator responsive-image" src="/lib/slike/banner-1.jpg">
                </div>
                <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">HammerFall Rock of Ages!
                <i class="material-icons right">expand_less</i>
              </span>
                    <p>
                <span class="">Location: Vienna
                  <br/>
                </span>
                        <a href="">more...</a>
                    </p>
                </div>
                <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">
                <i class="material-icons right">expand_more</i>HammerFall Rock of Ages!</span>
                    <p>Location: Vienna
                        <br/> Price: 25€
                        <br/> Who: HammerFall
                        <br/> Organization: Rock of Ages
                        <br/>
                    <p class="">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Labore velit nihil consectetur temporibus quae
                        praesentium, soluta ratione ex dolorum unde illo sequi laudantium doloribus veniam ab ad aperiam vero at.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum libero ut sunt tenetur doloribus consequuntur
                        nesciunt nemo, laboriosam fuga. Nihil optio reiciendis sed officiis sit sapiente incidunt eligendi quidem
                        excepturi?
                        <a href=""></a>
                    </p>
                    <button class="btn valign-wrapper right blue lighten-1 waves-effect">
                        <div class="valign-wrapper"> info &nbsp
                            <i class="material-icons">add_circle</i>
                        </div>
                    </button>
                    </p>
                </div>
            </div>
        </div>-->
        <!-- Konec ENE Karte-->
        <!-- Začetek ENE Karte-->
       <!-- <div class="col s12 m6 l4">
            <div class="card hoverable">
                <div class="card-image waves-effect waves-block">
                    <img class="activator responsive-image" src="/lib/slike/banner-1.jpg">
                </div>
                <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">HammerFall Rock of Ages!
                <i class="material-icons right">expand_less</i>
              </span>
                    <p>
                <span class="">Location: Vienna
                  <br/>
                </span>
                        <a href="">more...</a>
                    </p>
                </div>
                <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">
                <i class="material-icons right">expand_more</i>HammerFall Rock of Ages!</span>
                    <p>Location: Vienna
                        <br/> Price: 25€
                        <br/> Who: HammerFall
                        <br/> Organization: Rock of Ages
                        <br/>
                    <p class="">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Labore velit nihil consectetur temporibus quae
                        praesentium, soluta ratione ex dolorum unde illo sequi laudantium doloribus veniam ab ad aperiam vero at.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum libero ut sunt tenetur doloribus consequuntur
                        nesciunt nemo, laboriosam fuga. Nihil optio reiciendis sed officiis sit sapiente incidunt eligendi quidem
                        excepturi?
                        <a href=""></a>
                    </p>
                    <button class="btn valign-wrapper right blue lighten-1 waves-effect">
                        <div class="valign-wrapper"> info &nbsp
                            <i class="material-icons">add_circle</i>
                        </div>
                    </button>
                    </p>
                </div>
            </div>
        </div>-->
        <!-- Konec ENE Karte-->

        <!-- Konec Vrstice----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    </div>
<footer class="page-footer teal lighten-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">EV3NT5</h5>
                <p class="grey-text text-lighten-4">Brought to you by a team of enthusiastic students</p>
                <p class="grey-text text-lighten-4"><a class="grey-text text-lighten-4" href="https://gitlab.com/dejangregorc/eventsystem">Check our progress on Git</a></p>
            </div>
            <div class="col l6 s12">
                <ul>
                    <li class="grey-text text-lighten-3"><h5>Brought to you by</h5></li>
                    <li class="grey-text text-lighten-3">Faculty of Informatics</li>
                    <li class="grey-text text-lighten-3">Kaunas University of Technology</li>
                    <li class="grey-text text-lighten-3">Software Engineering</li>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright Eventisi
            <a class="grey-text text-lighten-4 right" href="index">EV3NT5</a>
        </div>
    </div>
</footer>
</body>
<script type="text/javascript" src="/lib/javascript/materialize.min.js"></script>
<script>
    // Every page needs this dingy //
    $(document).ready(function () {
        // Every page needs this dingy //
            $('.dropdown-button').dropdown({
                constrainWidth: false,
                hover: true,
                belowOrigin: true,
                alignment: 'left'
            });
            // Navbar //
            $('.button-collapse').sideNav();
            $('.dropdown-trigger').dropdown({
                constrainWidth: false,
                hover: true
            });

            // Page Specific //
            $('.datepicker').datepicker();


        // Select list //
        $('select').material_select();
    });
</script>

</html>