package si.feri.ost.ost.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import si.feri.ost.ost.demo.DAO.EventDAO;
import si.feri.ost.ost.demo.DAO.UserDAO;
import si.feri.ost.ost.demo.Models.Event;
import si.feri.ost.ost.demo.Models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    UserDAO user;
    @Autowired
    EventDAO events;
//Integer.parseInt(1)
//@RequestParam(value="userId",required=true)String userId,
//@RequestParam(value="useremail",required=true)String userEmail

    @RequestMapping(value={"/profile"},method=RequestMethod.GET)
    public String getUserData(Model model)
    {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        List<User> users=user.getByEmail(String.valueOf(session.getAttribute("username")));
        model.addAttribute("user",user.getByEmail(String.valueOf(session.getAttribute("username"))));
        model.addAttribute("events", events.getByUser(users.get(0).getId()));

        return "profile";
    }

    @RequestMapping(value={"/register"},method=RequestMethod.POST)
    public String register(Model model,
                           @RequestParam(value="name",required=false)String name,
                           @RequestParam(value="surname",required=false)String surname,
                           @RequestParam(value="username",required=false)String username,
                           @RequestParam(value="password",required=false)String password,
                           @RequestParam(value="phone",required=false)String contact,
                           @RequestParam(value="date",required=false)String date,
                           @RequestParam(value="country",required=false)String country

    )
    {

            user.addUser(name,surname,contact,date,username,password,1);


        return "index";

    }

    @RequestMapping(value={"/updateUser"},method=RequestMethod.POST)
    public String register(Model model,
                           @RequestParam(value="nameU",required=false)String name,
                           @RequestParam(value="surnameU",required=false)String surname,
                           @RequestParam(value="passwordU",required=false)String password,
                           @RequestParam(value="phoneU",required=false)String contact,
                           @RequestParam(value="countryU",required=false)String country,
                           @RequestParam(value="editMe", required = false)String id
    )
    {

        user.updateUser(Integer.parseInt(id),name,surname,contact,password,1);
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        List<User> user1 = user.getById(Integer.parseInt(id));
        session.setAttribute("idUser", user1.get(0).getId());
        session.setAttribute("name", user1.get(0).getName());
        session.setAttribute("surname", user1.get(0).getSurname());
        session.setAttribute("username", user1.get(0).getUsername());
        List<User> users=user.getByEmail(String.valueOf(session.getAttribute("username")));
        model.addAttribute("user",user.getByEmail(String.valueOf(session.getAttribute("username"))));
        model.addAttribute("events", events.getByUser(users.get(0).getId()));
        return "profile";


    }

    @RequestMapping(value = {"/editUser"}, method = RequestMethod.GET)
    public String prikaziNastavitve(Model model,
                                    @RequestParam(value = "eUser", required = false) String id) {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);

        model.addAttribute("editingUser", true);


        model.addAttribute("editedUser", user.getById(Integer.parseInt(id)));


        return "editUser";


    }
}
