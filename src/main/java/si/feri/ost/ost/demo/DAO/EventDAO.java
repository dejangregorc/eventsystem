package si.feri.ost.ost.demo.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import si.feri.ost.ost.demo.Models.Event;
import si.feri.ost.ost.demo.Models.User;
import si.feri.ost.ost.demo.UsefulClasses.DateTimeMethods;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Component
public class EventDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Event> getAllEvents()
    {
        String sql = "SELECT * FROM EVENT;";
        List<Event>  list = new ArrayList<>();

        List<Map<String,Object>> rows  = jdbcTemplate.queryForList(sql);

        for(Map<String,Object> row: rows){
            int id=(Integer)row.get("id_event");
            String title = (String)row.get("title");
            String description=(String)row.get("description");
            int country=(Integer)row.get("country_id_country");
            int user=(Integer)row.get("user_id_user");
            int type=(Integer)row.get("type_id_type");
            String location = (String)row.get("address");
            String date =String.valueOf((Date)row.get("date"));
            String time = String.valueOf((Time)row.get("time"));
            String price = String.valueOf((Integer)row.get("price"));
            list.add(new Event(id,title,description,country,user,type,location,date,time,price));
        }

        return list;

    }

    public List<Event> getFilteredEvents(String title, String location, String date, String maxPrice)
    {
        title="%"+title+"%";
        location="%"+location+"%";
        System.out.println(title);
        System.out.println(date);
        System.out.println(location);


        int price;
        if(date=="")
        {
            date="1998-12-31";
        }
        else date=DateTimeMethods.reworkDate(date);
        if(maxPrice=="")
        {
            price=10000;
        }
        else
        {
             price = Integer.parseInt(maxPrice);
        }
        String sql="SELECT * FROM EVENT WHERE TITLE LIKE ? AND ADDRESS LIKE ? AND PRICE<=? AND DATE >=?";// AND PRICE<=? AND DATE<?";

        List<Event>  list = new ArrayList<>();

        List<Map<String,Object>> rows  = jdbcTemplate.queryForList(sql,new Object[] {title,location, price, date});

        for(Map<String,Object> row: rows){
            int id=(Integer)row.get("id_event");
            title=(String)row.get("title");
            String description=(String)row.get("description");
            int country=(Integer)row.get("country_id_country");
            int user=(Integer)row.get("user_id_user");
            int type=(Integer)row.get("type_id_type");
            list.add(new Event(id,title,description,country,user,type));
        }
        System.out.println(list.size());
        return list;
    }

    public int addEvent(String title, String description, int country, int user, int type,String date,String time,String price,String location)
    {
     String sql = "INSERT INTO EVENT(title,description,country_id_country,user_id_user,type_id_type,date,time,address,price) VALUES(?,?,?,?,?,?,?,?,?)";

     return jdbcTemplate.update(sql,new Object[]{title, description, country, user, type,date,time,location,price});
    }


    public int updateDogodek(int id,String title, String description, int country, int user, int type){
        String sql = "UPDATE EVENT SET title=?,description=?,country_id_country=?,user_id_user=?,type_id_type=? WHERE id=?";

        return jdbcTemplate.update(sql,new Object[]{title, description, country, user, type,id});


    }

    public int updateEvent(int id, String title, String description, String date, String time, String address, String price)
    {
        String sql="UPDATE EVENT SET title=?, description=?, date=?, time=?, address=?, price=? WHERE id_event=?";

        return jdbcTemplate.update(sql, new Object[]{title, description, date, time, address, price, id});
    }

    public Object deleteEvent(int id)
    {
        String sql = "DELETE FROM EVENT WHERE id_event=?";
        System.out.println("heh"+id);
        return jdbcTemplate.update(sql,new Object[]{id});
    }

    public Event getByNaziv(String naziv)
    {
        String sql = "SELECT * FROM EVENT WHERE title=? ";

        Event d= (Event)jdbcTemplate.queryForObject(sql,
                new Object[] {naziv},
                new BeanPropertyRowMapper(Event.class));


        return d;

    }



    public List<Event> getByType(int type)
    {
        String sql = "SELECT * FROM EVENT WHERE type_id_type=?";
        List<Event> list = new ArrayList<>();

        List<Map<String,Object>> rows  = jdbcTemplate.queryForList(sql,new Object[]{type});

        for(Map row: rows){

            int id=(Integer)row.get("id_event");
            String title = (String)row.get("title");
            String description=(String)row.get("description");
            int country=(Integer)row.get("country_id_country");
            int user=(Integer)row.get("user_id_user");
            list.add(new Event(id,title,description,country,user,type));
        }

        return list;


    }

    public List<Event> getByUser(int id)
    {
        String sql = "SELECT * FROM EVENT WHERE user_id_user=?";
        List<Event> list = new ArrayList<>();

        List<Map<String,Object>> rows  = jdbcTemplate.queryForList(sql,new Object[]{id});

        for(Map row: rows){
            int eventId=(Integer)row.get("id_event");
            String title = (String)row.get("title");
            String description=(String)row.get("description");
            int country=(Integer)row.get("country_id_country");
            int user=(Integer)row.get("user_id_user");
            int type=(Integer)row.get("type_id_type");
            String location = (String)row.get("address");
            String date =((Date)row.get("DATE")).toString();
            String time = ((Time)row.get("time")).toString();
            String price;
            try {
                price = ((Integer) row.get("PRICE")).toString();
            }
            catch(Exception e)
            {
                price="0";
            }

            list.add(new Event(eventId,title,description,country,user,type,location,date,time,price));

        }
        return list;
    }

    public Event getById(int id)
    {
        String sql = "SELECT * FROM EVENT WHERE id_event=? ";

        List<Event> list = new ArrayList<>();

        List<Map<String,Object>> rows  = jdbcTemplate.queryForList(sql,new Object[]{id});

        for(Map row: rows){
            int eventId=id;
            String title = (String)row.get("title");
            String description=(String)row.get("description");
            int country=(Integer)row.get("country_id_country");
            int user=(Integer)row.get("user_id_user");
            int type=(Integer)row.get("type_id_type");
            String location = (String)row.get("address");
            String date =((Date)row.get("DATE")).toString();
            String time = ((Time)row.get("time")).toString();

            String price;
            try {
                price = ((Integer) row.get("PRICE")).toString();
            }
            catch(Exception e)
            {
                price="0";
            }
            list.add(new Event(eventId,title,description,country,user,type,location,date,time,price));

        }
        try {
            return list.get(0);
        }
        catch (Exception e){
            return null;
        }
    }

    public List<Event> getByTitle(String title)
    {
        String sql = "SELECT * FROM EVENT WHERE title=?";
        List<Event> list = new ArrayList<>();

        List<Map<String,Object>> rows  = jdbcTemplate.queryForList(sql,new Object[]{title});

        for(Map row: rows){
            int eventId=(Integer)row.get("id_event");

            String description=(String)row.get("description");
            int country=(Integer)row.get("country_id_country");
            int user=(Integer)row.get("user_id_user");
            int type=(Integer)row.get("type_id_type");
            String location = (String)row.get("address");
            String date =((Date)row.get("DATE")).toString();
            String time = ((Time)row.get("time")).toString();
            String price;
            try {
                price = ((Integer) row.get("PRICE")).toString();
            }
            catch(Exception e)
            {
                price="0";
            }

            list.add(new Event(eventId,title,description,country,user,type,location,date,time,price));

        }
        return list;
    }

    public void save(MultipartFile imageFile, String title)throws Exception {

        Event e= getByTitle(title).get(0);
        System.out.println();
        System.out.println();
        System.out.println(e.getId());
        System.out.println();
        System.out.println();
        String folder="src/main/webapp/lib/slike/";
        byte[] bytes = imageFile.getBytes();
        Path path = Paths.get(folder +e.getId()+".jpg");
        //imageFile.getOriginalFilename()
        Files.write(path,bytes);


    }
}
