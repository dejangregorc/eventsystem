package si.feri.ost.ost.demo.Models;

public class Term {
    private int id;
    private String date;
    private String time;
    private String address;
    private int price;
    private int event;

    public Term(){}

    public Term(int id, String date, String time, String address, int price, int event)
    {
    this.id=id;
    this.date=date;
    this.time=time;
    this.address=address;
    this.price=price;
    this.event=event;
    }
    public Term(String date, String time, String address, int price, int event)
    {
        this.date=date;
        this.time=time;
        this.address=address;
        this.price=price;
        this.event=event;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }


}
