CREATE TABLE IF NOT EXISTS `Country` (
  `id_country` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_country`));




CREATE TABLE IF NOT EXISTS `User` (
  `id_user` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `surname` VARCHAR(200) NOT NULL,
  `phone` VARCHAR(45) NULL,
  `birth_date` DATE NULL,
  `username` VARCHAR(200) NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  `Country_id_country` INT NOT NULL,
  PRIMARY KEY (`id_user`),
  CONSTRAINT `fk_User_Country1`
    FOREIGN KEY (`Country_id_country`)
    REFERENCES `Country` (`id_country`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



CREATE TABLE IF NOT EXISTS `Type` (
  `id_type` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NULL,
  PRIMARY KEY (`id_type`));



CREATE TABLE IF NOT EXISTS `Event` (
  `id_event` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(2000) NOT NULL,
  `description` VARCHAR(2000) NULL,
  `Country_id_country` INT NOT NULL,
  `User_id_user` INT NOT NULL,
  `Type_id_type` INT NOT NULL,
  PRIMARY KEY (`id_event`),
  CONSTRAINT `fk_Event_Country1`
    FOREIGN KEY (`Country_id_country`)
    REFERENCES `Country` (`id_country`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Event_User1`
    FOREIGN KEY (`User_id_user`)
    REFERENCES `User` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Event_Type1`
    FOREIGN KEY (`Type_id_type`)
    REFERENCES `Type` (`id_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


CREATE TABLE IF NOT EXISTS `Term` (
  `id_term` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NULL,
  `time` TIME NULL,
  `address` VARCHAR(2000) NOT NULL,
  `price` INT NOT NULL,
  `Event_id_event` INT NOT NULL,
  PRIMARY KEY (`id_term`),
 
  CONSTRAINT `fk_Term_Event1`
    FOREIGN KEY (`Event_id_event`)
    REFERENCES `Event` (`id_event`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



CREATE TABLE IF NOT EXISTS `SavedEvent` (
  `id_saved_event` INT NOT NULL AUTO_INCREMENT,
  `User_id_user` INT NOT NULL,
  `Event_id_event` INT NOT NULL,
  PRIMARY KEY (`id_saved_event`),
  CONSTRAINT `fk_SavedEvent_User`
    FOREIGN KEY (`User_id_user`)
    REFERENCES `User` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SavedEvent_Event1`
    FOREIGN KEY (`Event_id_event`)
    REFERENCES `Event` (`id_event`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



