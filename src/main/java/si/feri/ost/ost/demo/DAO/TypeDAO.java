package si.feri.ost.ost.demo.DAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import si.feri.ost.ost.demo.Models.Type;

@Component
public class TypeDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public int addType(String name)
    {
        String sql = "INSERT INTO Type(name) VALUES(?)";
        return jdbcTemplate.update(sql,new Object[]{name});
    }

    public Object deleteType(int id)
    {
        String sql = "DELETE FROM Type WHERE id=?";
        return jdbcTemplate.update(sql,new Object[]{id});
    }

    public Type getByName(String name)
    {
        String sql = "SELECT * FROM Type WHERE name=? ";
        Type d= (Type)jdbcTemplate.queryForObject(sql,
                new Object[] {name},
                new BeanPropertyRowMapper(Type.class));
        return d;
    }

    public Type getById(int id)
    {
        String sql = "SELECT * FROM Type WHERE id_Type=? ";
        Type d= (Type)jdbcTemplate.queryForObject(sql,
                new Object[] {id},
                new BeanPropertyRowMapper(Type.class));
        return d;
    }


}
