<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EV3NT5</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/lib/css/materialize.min.css" media="screen,projection" />
    <link rel="stylesheet" type="text/css" media="screen" href="/lib/css/style.css" />

</head>

<body>
<main class="teal lighten-5">
    <div class="navbar-fixed">
        <nav class=" teal darken-2 z-depth-3">
            <div class="nav-wrapper">
                <a href="index" class="brand-logo">EV3NT5</a>
                <a href="#" class="button-collapse" data-activates="mobile-sidenav">
                    <i class="material-icons">menu</i>
                </a>
                <ul class="right show-on-med-and-down">
                    <li>
                        <a href="mapEvents">
                            <i class="material-icons">place</i>
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-trigger" data-activates="dropdownPerson" href="#">
                            <i class="material-icons left">person</i>
                        </a>
                        <ul id='dropdownPerson' class='dropdown-content'>
                            <%if(session.getAttribute("username")!=null)
                            {%>
                            <li>
                                <a href="profile">
                                    <button class="btn-flat teal-text">My events</button>
                                </a>

                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="addEvent">
                                    <button class="btn-flat teal-text">Add event</button>
                                </a>
                            </li>
                            <li>
                                <a href="/logout">
                                    <button class="btn-flat teal-text">Log out</button>
                                </a>
                            </li>
                            <% }else{%>
                            <li class="divider"></li>
                            <li>
                                <a href="loginPage">
                                    <button class="btn-flat teal-text">Sign in</button>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="signup">
                                    <button class="btn-flat teal-text">Sign up</button>
                                </a>
                            </li>
                            <% } %>
                        </ul>
                    </li>
                </ul>
                <ul id="nav-mobile" class="right hide-on-med-and-down">

                    <li class="active">
                        <a href="index">Home</a>
                    </li>
                    <li>
                    <li>
                        <a href="events">Events</a>
                    </li>
                </ul>
                <ul class="side-nav" id="mobile-sidenav">
                    <li>
                        <a href="events">Events</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <h1 class="center-align col s12">EV3NT5</h1>
            <hr/>
        </div>
        <div class="carousel carousel-slider hoverable z-depth-4" data-indicators="true">
            <a class="carousel-item" href="#one!">
                <img src="/lib/slike/hkphil1718.JPG">
            </a>
            <a class="carousel-item" href="#two!">
                <img src="/lib/slike/bkpam2284930_gallery6.jpeg">
            </a>
            <a class="carousel-item" href="#three!">
                <img src="/lib/slike/banner-1.jpg">
            </a>
            <a class="carousel-item" href="#four!">
                <img src="/lib/slike/iron-maiden.jpg">
            </a>
            <a class="carousel-item" href="#five!">
                <img src="/lib/slike/zalgiris-kaunas.jpg">
            </a>
        </div>
        <div class="row">
            <div class="col s8">
                <h3 class="center-align">About us</h3>
                <p class="flow-text center-align">In the context of event organization, we increasingly need software that can analyze needs and provide
                    solutions in real time. As a result, our platform will plan and organize any event with your criteria. Thus the tasks will be simplified
                    and there will be a considerable time saving in the organization of your projects. so no more worries of delay or planning your events because
                    our platform event app will take care of everything.
                </p>
            </div>
            <div class="col s4">
                <h3 class="center-align">Contacts</h3>
                <div class="grey lighten-3">
                    <ul class="collection z-depth-1">
                        <li class="collection-item avatar grey lighten-3">
                            <img src="" alt="" class="circle">
                            <span class="title">Cenker Canbulut</span>
                            <p>
                                Stakeholder
                                <br/> cenker.canbulut@ktu.edu
                            </p>
                            <a href="#!" class="secondary-content">
                                <i class="material-icons red-text text-lighten-2">send</i>
                            </a>
                        </li>
                        <li class="collection-item avatar grey lighten-3">
                            <img src="" alt="" class="circle">
                            <span class="title">Nina Kliček</span>
                            <p>
                                Project Owner
                                <br/>Front-End
                                <br/>ninkli@ktu.lt
                            </p>
                            <a href="#!" class="secondary-content">
                                <i class="material-icons red-text text-lighten-2">send</i>
                            </a>
                        </li>
                        <li class="collection-item avatar grey lighten-3">
                            <img src="" alt="" class="circle">
                            <span class="title">Dejan Gregorc</span>
                            <p>
                                Scrum Master
                                <br/>Back-End
                                <br/> dejgre@ktu.lt
                            </p>
                            <a href="#!" class="secondary-content">
                                <i class="material-icons red-text text-lighten-2">send</i>
                            </a>
                        </li>
                        <li class="collection-item avatar grey lighten-3">
                            <img src="" alt="" class="circle">
                            <span class="title">Fatou Kine Ndiaye</span>
                            <p>
                                Documentation
                                <br/>fatndy@ktu.lt
                            </p>
                            <a href="#!" class="secondary-content">
                                <i class="material-icons red-text text-lighten-2">send</i>
                            </a>
                        </li>
                        <li class="collection-item avatar grey lighten-3">
                            <img src="" alt="" class="circle">
                            <span class="title">Prudencio Slim</span>
                            <p>
                                Documentation
                                <br/>prusli@ktu.lt
                            </p>
                            <a href="#!" class="secondary-content">
                                <i class="material-icons red-text text-lighten-2">send</i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</main>
<footer class="page-footer teal lighten-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">EV3NT5</h5>
                <p class="grey-text text-lighten-4">Brought to you by a team of enthusiastic students</p>
                <p class="grey-text text-lighten-4"><a class="grey-text text-lighten-4" href="https://gitlab.com/dejangregorc/eventsystem">Check our progress on Git</a></p>
            </div>
            <div class="col l6 s12">
                <ul>
                    <li class="grey-text text-lighten-3"><h5>Brought to you by</h5></li>
                    <li class="grey-text text-lighten-3">Faculty of Informatics</li>
                    <li class="grey-text text-lighten-3">Kaunas University of Technology</li>
                    <li class="grey-text text-lighten-3">Software Engineering</li>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright Eventisi
            <a class="grey-text text-lighten-4 right" href="index">EV3NT5</a>
        </div>
    </div>
</footer>
</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" src="/lib/javascript/materialize.min.js"></script>

<script>

    // Every page needs this dingy //
    $(document).ready(function () {
        $('.dropdown-button').dropdown({
            constrainWidth: false,
            hover: true,
            belowOrigin: true,
            alignment: 'left'
        });
        // Navbar //
        $('.button-collapse').sideNav();
        $('.dropdown-trigger').dropdown({
            constrainWidth: false,
            hover: true
        });

        // Page Specific //

        // Carousel //
        $('.carousel.carousel-slider').carousel({
            fullWidth: true,
            noWrap: true
        });



    });
</script>

</html>