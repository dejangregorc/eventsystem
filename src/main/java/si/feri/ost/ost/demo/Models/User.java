package si.feri.ost.ost.demo.Models;

public class User {

    private int id_user;
    private String name;
    private String surname;
    private String phone;
    private String birthDate;
    private String username;
    private String password;
    private int country;

public User(int id, String name, String surname)
{
    this.id_user=id;
    this.name=name;
    this.surname=surname;
}

    public User(int id_user, String name, String surname, String phone, String birthDate, String username, String password) {
        this.id_user = id_user;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.birthDate = birthDate;
        this.username = username;
        this.password = password;
    }

    public User(int id_user, String name, String surname, String phone, String birthDate, String username) {
        this.id_user = id_user;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.birthDate = birthDate;
        this.username = username;
    }

    public User(int id, String name, String surname, String username, String password)
    {
        this.id_user=id;
        this.name=name;
        this.surname=surname;
        this.username=username;
        this.password=password;
    }
    public User(String name, String surname,String username,String password)
    {
        this.name=name;
        this.surname=surname;
        this.username=username;
        this.password=password;
    }
public User(){};

    public User(int id, String name, String surname, String phone, String birthDate, String username, String password, int country) {
        this.id_user=id;
        this.name=name;
        this.surname=surname;
        this.phone=phone;
        this.birthDate=birthDate;
        this.username=username;
        this.password=password;
        this.country=country;

    }
    public User(String name, String surname, String phone, String birthDate, String username, String password, int country) {

        this.name=name;
        this.surname=surname;
        this.phone=phone;
        this.birthDate=birthDate;
        this.username=username;
        this.password=password;
        this.country=country;

    }


    public int getId() {
        return id_user;
    }

    public void setId(int id) {
        this.id_user = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Ime='" + name + '\'' +
                ", Priimek='" + surname + '\'' +

                '}';
    }
}


