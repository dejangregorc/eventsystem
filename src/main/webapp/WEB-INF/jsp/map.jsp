<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EV3NT5</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/lib/css/materialize.min.css" media="screen,projection" />
    <link rel="stylesheet" type="text/css" media="screen" href="/lib/css/style.css" />
    <style>
        #map {
            height: 800px;
            width: 100%;
        }
    </style>
</head>

<body>
<main class="teal lighten-5">
    <div class="navbar-fixed">
        <nav class=" teal darken-2 z-depth-3">
            <div class="nav-wrapper">
                <a href="index" class="brand-logo">EV3NT5</a>
                <a href="#" class="button-collapse" data-activates="mobile-sidenav">
                    <i class="material-icons">menu</i>
                </a>
                <ul class="right show-on-med-and-down">
                    <li>
                        <a href="mapEvents">
                            <i class="material-icons">place</i>
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-trigger" data-activates="dropdownPerson" href="#">
                            <i class="material-icons left">person</i>
                        </a>
                        <ul id='dropdownPerson' class='dropdown-content'>
                            <%if(session.getAttribute("username")!=null)
                            {%>
                            <li>
                                <a href="profile">
                                    <button class="btn-flat teal-text">My events</button>
                                </a>

                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="addEvent">
                                    <button class="btn-flat teal-text">Add event</button>
                                </a>
                            </li>
                            <li>
                                <a href="/logout">
                                    <button class="btn-flat teal-text">Log out</button>
                                </a>
                            </li>
                            <% }else{%>
                            <li class="divider"></li>
                            <li>
                                <a href="loginPage">
                                    <button class="btn-flat teal-text">Sign in</button>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="signup">
                                    <button class="btn-flat teal-text">Sign up</button>
                                </a>
                            </li>
                            <% } %>
                        </ul>
                    </li>
                </ul>
                <ul id="nav-mobile" class="right hide-on-med-and-down">

                    <li class="active">
                        <a href="index">Home</a>
                    </li>
                    <li>
                    <li>
                        <a href="events">Events</a>
                    </li>
                </ul>
                <ul class="side-nav" id="mobile-sidenav">
                    <li>
                        <a href="events">Events</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">

        <div class="row">
            <c:choose>
                <c:when test="${single==true}">
                    <h2 class="center-align">Location of ${title} </h2>
                </c:when>

                <c:otherwise>
                    <h2 class="center-align">All events around you </h2>
                </c:otherwise>
            </c:choose>

            <div id="map"></div>
            <script>
                var izbira = "${fullMap}";
                var lokacije = "${locations}";
                var locations=new Array();
                var title="${titles}";
                var titles=new Array();

                var x="";
                for (var i = 0; i < lokacije.length; i++) {
                    if (lokacije[i] != ',')
                        x += lokacije[i];
                    else if (lokacije[i] === ',') {
                        locations.push(x);
                        x = "";
                    }
                    if (i === lokacije.length - 2)
                        locations.push(x);
                }



                var geocoder;
                var map;
                var address = "${naslovLokacije}";
                function initMap() {
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 5,
                        center: {lat: 54.898521, lng:23.903597}
                    });
                    geocoder = new google.maps.Geocoder();
                    if(izbira==='true') {
                        for (var i = 0; i < locations.length; i++) {
                            setInfoWindow(i);
                        }
                        function setInfoWindow(i) {
                            geocoder.geocode({'address': locations[i]}, function (results, status) {
                                if (status === 'OK') {
                                    map.setCenter(46.245013, 15.259217);
                                    map.setZoom(8.7);
                                    var marker = new google.maps.Marker({
                                        map: map,
                                        position: results[0].geometry.location
                                    });
                                    marker.addListener('click', function() {
                                        var infowindow = new google.maps.InfoWindow({
                                            content: '<div id="content" class="flow-text" style=padding:16px; ;border-radius:5px;max-width:260px;box-shadow:none;">'
                                            +locations[i]+'<br><br>  ' +
                                            '<form action="/dogodkiPoLokaciji" method="get"><button type="submit"class="btn valign-wrapper right blue lighten-1 waves-effect" id="button" value="'+locations[i]+'" name="lokacija">Prikaži dogodke</button></form> ' +
                                            '</div>'
                                        });
                                        infowindow.open(map, marker);
                                    });
                                } else {
                                    alert('Prikaz na zemljevidu neuspešen zaradi: ' + status);
                                }
                            });
                        }
                    }
                    else{
                        codeAddress(geocoder, map);
                    }
                }
                function codeAddress(geocoder, map) {
                    geocoder.geocode({'address': address}, function(results, status) {
                        if (status === 'OK') {
                            map.setCenter(results[0].geometry.location);
                            var marker = new google.maps.Marker({
                                map: map,
                                position: results[0].geometry.location
                            });
                        } else {
                            alert('Prikaz na zemljevidu neuspešen zaradi: ' + status);
                        }
                    });
                }

            </script>
            <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqDzfA85d4SVOfcH-NKKeRrWY5OMP480Y&callback=initMap">
            </script>

        </div>
    </div>

</main>
<footer class="page-footer teal lighten-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">EV3NT5</h5>
                <p class="grey-text text-lighten-4">Brought to you by a team of enthusiastic students</p>
                <p class="grey-text text-lighten-4"><a class="grey-text text-lighten-4" href="https://gitlab.com/dejangregorc/eventsystem">Check our progress on Git</a></p>
            </div>
            <div class="col l6 s12">
                <ul>
                    <li class="grey-text text-lighten-3"><h5>Brought to you by</h5></li>
                    <li class="grey-text text-lighten-3">Faculty of Informatics</li>
                    <li class="grey-text text-lighten-3">Kaunas University of Technology</li>
                    <li class="grey-text text-lighten-3">Software Engineering</li>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright Eventisi
            <a class="grey-text text-lighten-4 right" href="index">EV3NT5</a>
        </div>
    </div>
</footer>
</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" src="/lib/javascript/materialize.min.js"></script>

<script>

    // Every page needs this dingy //
    $(document).ready(function () {
        $('.dropdown-button').dropdown({
            constrainWidth: false,
            hover: true,
            belowOrigin: true,
            alignment: 'left'
        });
        // Navbar //
        $('.button-collapse').sideNav();
        $('.dropdown-trigger').dropdown({
            constrainWidth: false,
            hover: true
        });

        // Page Specific //

        // Carousel //
        $('.carousel.carousel-slider').carousel({
            fullWidth: true,
            noWrap: true
        });



    });
</script>

</html>