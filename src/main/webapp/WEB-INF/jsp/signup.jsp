<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="sl">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EV3NT5</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/lib/css/materialize.min.css" media="screen,projection" />
    <link rel="stylesheet" type="text/css" media="screen" href="/lib/css/style.css" />

</head>
<body>
<div class="teal lighten-5">
    <div class="navbar-fixed">
        <nav class=" teal darken-2 z-depth-3">
            <div class="nav-wrapper">
                <a href="index" class="brand-logo">EV3NT5</a>
                <a href="#" class="button-collapse" data-activates="mobile-sidenav">
                    <i class="material-icons">menu</i>
                </a>
                <ul class="right show-on-med-and-down">
                    <li>
                        <a href="mapEvents">
                            <i class="material-icons">place</i>
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-trigger" data-activates="dropdownPerson" href="#">
                            <i class="material-icons left">person</i>
                        </a>
                        <ul id='dropdownPerson' class='dropdown-content'>
                            <%if(session.getAttribute("username")!=null)
                            {%>
                            <li>
                                <a href="profile">
                                    <button class="btn-flat teal-text">My events</button>
                                </a>

                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="addEvent">
                                    <button class="btn-flat teal-text">Add event</button>
                                </a>
                            </li>
                            <li>
                                <a href="/logout">
                                    <button class="btn-flat teal-text">Log out</button>
                                </a>
                            </li>
                            <% }else{%>
                            <li class="divider"></li>
                            <li>
                                <a href="loginPage">
                                    <button class="btn-flat teal-text">Sign in</button>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="signup">
                                    <button class="btn-flat teal-text">Sign up</button>
                                </a>
                            </li>
                            <% } %>
                        </ul>
                    </li>
                </ul>
                <ul id="nav-mobile" class="right hide-on-med-and-down">

                    <li class="active">
                        <a href="index">Home</a>
                    </li>
                    <li>
                    <li>
                        <a href="events">Events</a>
                    </li>
                </ul>
                <ul class="side-nav" id="mobile-sidenav">
                    <li>
                        <a href="events">Events</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <h2 class="center-align">

                    Sign up
        </h2>
                <form action="/register" method="post">

                        <div class="row">
                            <div class="input-field col s10 offset-s1 col l8 offset-l2">
                                <input id="name" name="name" type="text" class="validate" >
                                <label for="name">Name*:</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s10 offset-s1 col l8 offset-l2">
                                <input id="surname" name="surname" type="text" class="validate" >
                                <label for="surname">Surname*:</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s10 offset-s1 col l8 offset-l2">
                                <input id="email" name="username" type="text" class="validate" >
                                <label for="email">Email*:</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s10 offset-s1 col l8 offset-l2">
                                <input id="pass1" name="password" type="password" class="validate" >
                                <label for="pass1">Password*:</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s10 offset-s1 col l8 offset-l2">
                                <input id="pass2" type="password" class="validate">
                                <label for="pass2">Repeat password*:</label>
                                <span id='message'></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s10 offset-s1 col l8 offset-l2">
                                <input id="phone" name="phone" type="text" class="validate">
                                <label for="phone">Phone number:</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s10 offset-s1 col l8 offset-l2">
                                <input id="date" name="date" type="text" class="validate">
                                <label for="date">Date:</label>
                            </div>
                        </div>

                    <div class="row">
                        <div class="input-field col s10 offset-s1 col l8 offset-l2">
                            <input id="country" name="country" type="hidden" class="validate" type="hidden" value="1">
                        </div>
                    </div>
                        <div class="row">
                            <div class="input-field center-align">
                                        <button id="signIn" class="btn btn-submit large" type="submit">
                                            Sign me up
                                        </button>
                            </div>
                        </div>

                </form>
    </div>
</div>
<footer class="page-footer teal lighten-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">EV3NT5</h5>
                <p class="grey-text text-lighten-4">Brought to you by a team of enthusiastic students</p>
                <p class="grey-text text-lighten-4"><a class="grey-text text-lighten-4" href="https://gitlab.com/dejangregorc/eventsystem">Check our progress on Git</a></p>
            </div>
            <div class="col l6 s12">
                <ul>
                    <li class="grey-text text-lighten-3"><h5>Brought to you by</h5></li>
                    <li class="grey-text text-lighten-3">Faculty of Informatics</li>
                    <li class="grey-text text-lighten-3">Kaunas University of Technology</li>
                    <li class="grey-text text-lighten-3">Software Engineering</li>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright Eventisi
            <a class="grey-text text-lighten-4 right" href="index">EV3NT5</a>
        </div>
    </div>
</footer>
</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="/lib/javascript/materialize.min.js"></script>

<script>
    // Every page needs this dingy //
    $(document).ready(function () {
        $('.dropdown-button').dropdown({
            constrainWidth: false,
            hover: true,
            belowOrigin: true,
            alignment: 'left'
        });
        // Navbar //
        $('.button-collapse').sideNav();
        $('.dropdown-trigger').dropdown({
            constrainWidth: false,
            hover: true
        });

        // Page Specific //

    });
    $('#pass1, #pass2').on('keyup', function () {
        if ($('#pass1').val() == $('#pass2').val()) {
            $('#message').html('Passwords are matching').css('color', 'green');
        } else
            $('#message').html('Passwords are not matching').css('color', 'red');
    });


</script>

</html>