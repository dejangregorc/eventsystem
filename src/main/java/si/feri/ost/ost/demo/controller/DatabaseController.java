package si.feri.ost.ost.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import si.feri.ost.ost.demo.DAO.EventDAO;
import si.feri.ost.ost.demo.DAO.UserDAO;
import si.feri.ost.ost.demo.Models.Event;
import si.feri.ost.ost.demo.Models.User;
import si.feri.ost.ost.demo.UsefulClasses.DateTimeMethods;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class DatabaseController {
    @Autowired
    EventDAO dogodki;
    UserDAO user;

    @RequestMapping(value={"/Konzola",}, method=RequestMethod.GET)
    public String konzola(Model model)
    {

            model.addAttribute("dogodki",dogodki.getAllEvents());

        return "Konsola";
    }

    @RequestMapping(value={"/events",}, method=RequestMethod.GET)
    public String events(Model model)
    {
        model.addAttribute("events",dogodki.getAllEvents());
        return "events";
    }

    @RequestMapping(value="/editEvent", method=RequestMethod.POST)
    public String editEvent(Model model,
                            @RequestParam(value="id", required = true)String id)
    {
        Event event=dogodki.getById(Integer.parseInt(id));
        List<Event> events=new ArrayList<>();
        events.add(event);
        model.addAttribute("id", id);
        model.addAttribute("events",events);
        return "editEvent";
    }

    @RequestMapping(value="/edit",method=RequestMethod.POST)
            public String editingEvent(Model model,
                                       @RequestParam(value="id", required = true) String id,
                                       @RequestParam(value="title", required = true)String title,
                                       @RequestParam(value="description",required = true)String description,
                                       @RequestParam(value="date",required=true)String date,
                                       @RequestParam(value="time",required=false)String time,
                                       @RequestParam(value="price",required = false)String price,
                                       @RequestParam(value="location",required = false)String location,
                                       @RequestParam(value="file", required = false) MultipartFile imageFile)
    {
        String goodDate;
        String goodTime;
        try
        {
            goodDate=DateTimeMethods.reworkDate((date));
        }
        catch (Exception e)
        {
            goodDate=date;
        }


        dogodki.updateEvent(Integer.parseInt(id),title, description,goodDate, time,location, price);



        return "index";



    }


    @RequestMapping(value = {"/add" }, method = RequestMethod.POST)
    public String dodajDogodek(Model model, @RequestParam(value="title",required=true)String title,
                               @RequestParam(value="description",required = true)String description,
                               @RequestParam(value="date",required=true)String date,
                               @RequestParam(value="time",required=false)String time,
                               @RequestParam(value="price",required = false)String price,
                               @RequestParam(value="location",required = false)String location,
                               @RequestParam(value="file", required = false) MultipartFile imageFile
    )  {
        System.out.println(time);
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        System.out.println(session.getAttribute("username"));
        String username=session.getAttribute("username").toString();
        int id=Integer.parseInt(String.valueOf(session.getAttribute("idUser")));


        dogodki.addEvent(title,description,1,id,1, DateTimeMethods.reworkDate(date),DateTimeMethods.reworkTime(time),price,location);
        try {

            dogodki.save(imageFile,title);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR!!!!!!");
        }



        model.addAttribute("events",dogodki.getAllEvents());

        return "events";
    }


    @RequestMapping(value={"/filter"},method=RequestMethod.GET)
    public String filtriraj(Model model,
    @RequestParam (value="title", required=false) String title,
                            @RequestParam(value="location",required=false)String location,
                            @RequestParam(value="date",required=false)String date,
                            @RequestParam(value="maxPrice",required=false)String maxPrice
    )

    {
model.addAttribute("events",dogodki.getFilteredEvents(title,location,date,maxPrice));
        return "events";
    }



@RequestMapping(value={"/image"},method = RequestMethod.POST)
public String uploadImage(@RequestParam("file") MultipartFile imageFile){

    try {
        //dogodki.save(imageFile);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return "Konsola";
}






}
