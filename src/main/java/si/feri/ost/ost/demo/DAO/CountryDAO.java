package si.feri.ost.ost.demo.DAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import si.feri.ost.ost.demo.Models.Country;


@Component
public class CountryDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public int addCountry(String name)
    {
        String sql = "INSERT INTO COUNTRY(name) VALUES(?)";
        return jdbcTemplate.update(sql,new Object[]{name});
    }

    public Object deleteCountry(int id)
    {
        String sql = "DELETE FROM COUNTRY WHERE id=?";
        return jdbcTemplate.update(sql,new Object[]{id});
    }

    public Country getByName(String name)
    {
        String sql = "SELECT * FROM COUNTRY WHERE name=? ";
        Country d= (Country)jdbcTemplate.queryForObject(sql,
                new Object[] {name},
                new BeanPropertyRowMapper(Country.class));
        return d;
    }

    public Country getById(int id)
    {
        String sql = "SELECT * FROM COUNTRY WHERE id_country=? ";
        Country d= (Country)jdbcTemplate.queryForObject(sql,
                new Object[] {id},
                new BeanPropertyRowMapper(Country.class));
        return d;
    }


}
