package si.feri.ost.ost.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import si.feri.ost.ost.demo.DAO.EventDAO;
import si.feri.ost.ost.demo.DAO.UserDAO;
import si.feri.ost.ost.demo.Models.Event;
import si.feri.ost.ost.demo.Models.User;
import si.feri.ost.ost.demo.UsefulClasses.DateTimeMethods;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    UserDAO users;
    @Autowired
    UserDAO userLogin;
    @Autowired
    EventDAO eventsLogin;

    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {

        return "index";
    }



    //testni jsp za izpise ipd.
    @RequestMapping(value = { "/Test" }, method = RequestMethod.GET)
    public String test(Model model) {

        return "Konsola";
    }


    @RequestMapping(value = { "/loginPage" }, method = RequestMethod.GET)
    public String loginPage(Model model)
    {
        return "login";
    }

    @RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
    public String logout(Model model)
    {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        session.removeAttribute("idUser");
        session.removeAttribute("loginSuccess");
        session.removeAttribute("name");
        session.removeAttribute("surname");
        session.removeAttribute("username");

        return "login";
    }



    @RequestMapping(value = { "/login" }, method = RequestMethod.POST)
    public String login(Model model, @RequestParam(value="username",required=true)String username,
                               @RequestParam(value="password",required = true)String password

    )  {
        boolean loginSuccess=false;
        List<User>allUsers=users.getAllUsers();

User user=new User();
for(int i=0;i<allUsers.size();i++)
{
    if(allUsers.get(i).getUsername().equals(username)&&allUsers.get(i).getPassword().equals(password))
    {
    user=allUsers.get(i);
    loginSuccess=true;
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        session.setAttribute("username",username);
        session.setAttribute("idUser", user.getId());
        session.setAttribute("loginSuccess", loginSuccess);
        session.setAttribute("name", user.getName());
        session.setAttribute("surname", user.getSurname());
        session.setAttribute("avatarski", "https://api.adorable.io/avatars/200/+"+user.getName()+".png");

break;
    }
}
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            HttpSession session = request.getSession(true);

            session.setAttribute("idUser", user.getId());
            session.setAttribute("loginSuccess", loginSuccess);
            session.setAttribute("name", user.getName());
            session.setAttribute("surname", user.getSurname());
            session.setAttribute("username", user.getUsername());

        model.addAttribute("loginSuccess",loginSuccess);
        if(loginSuccess==true)
        {
            List<User> users=userLogin.getByEmail(String.valueOf(session.getAttribute("username")));
            model.addAttribute("user",userLogin.getByEmail(String.valueOf(session.getAttribute("username"))));
            model.addAttribute("events", eventsLogin.getByUser(users.get(0).getId()));
            return "profile";

        }
        else
            return "index";

    }


    @RequestMapping(value = { "/removeEvent" }, method = RequestMethod.POST)
    public String removeEvent(Model model, @RequestParam(value = "idEvent",required = true) String id) {
        System.out.println(id);
        int ID=Integer.parseInt(id);
        Event e =eventsLogin.getById(ID);
        File file= new File("src/main/webapp/lib/slike/"+e.getTitle()+".jpg");
        file.delete();
        eventsLogin.deleteEvent(ID);
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        List<User> users=userLogin.getByEmail(String.valueOf(session.getAttribute("username")));
        model.addAttribute("user",userLogin.getByEmail(String.valueOf(session.getAttribute("username"))));
        model.addAttribute("events", eventsLogin.getByUser(users.get(0).getId()));


        return "profile";
    }






    @RequestMapping(value = { "/addEvent" }, method = RequestMethod.GET)
    public String dodajanjeDogodka(Model model) {

        return "addEvent";
    }


    @RequestMapping(value = { "/signup" }, method = RequestMethod.GET)
    public String registracija(Model model) {

        return "signup";
    }



    @RequestMapping(value = { "/mapEvents" }, method = RequestMethod.GET)
    public String map(Model model, @RequestParam(value = "id", required = false)String id) {
        List<Event> vsiDogodki= new ArrayList<>();
        boolean single;
        if (id==null) {
            vsiDogodki = eventsLogin.getAllEvents();
            single=false;
        }
        else
        {
            vsiDogodki.add(eventsLogin.getById(Integer.parseInt(id)));
            single=true;
            model.addAttribute("title",vsiDogodki.get(0).getTitle());
        }
        HashSet<String> locations = new HashSet<>();
        List<String>titles=new ArrayList();

        for(int i=0; i<vsiDogodki.size();i++){
            locations.add(vsiDogodki.get(i).getLocation());
            titles.add(vsiDogodki.get(i).getTitle());
            System.out.println(vsiDogodki.get(i).getLocation()+", "+vsiDogodki.get(i).getTitle());
        }


        model.addAttribute("locations",locations);
        model.addAttribute("titles",titles);
        model.addAttribute("single",single);
        model.addAttribute("fullMap",true);

        return "map";
    }
}
